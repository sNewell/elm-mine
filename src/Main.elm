module Main exposing (Model, Msg(..), init, main, update)

import Browser
import Dict as Dict
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onClick)
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Html.Events exposing (preventDefaultOn)
import Json.Decode as Json
import Random
import Set as Set


onRightClick message =
    htmlAttribute (htmlOnRightClick message)


htmlOnRightClick : msg -> Html.Attribute msg
htmlOnRightClick message =
    preventDefaultOn
        "contextmenu"
        (Json.map alwaysPreventDefault (Json.succeed message))


alwaysPreventDefault : msg -> ( msg, Bool )
alwaysPreventDefault msg =
    ( msg, True )


black =
    Element.rgb 0 0 0


white =
    Element.rgb 1 1 1


red =
    Element.rgb 0.8 0 0


hoverBlue =
    Element.rgb 0.25 0.85 1.0


cellSize =
    px 34



---- MODEL ----


type alias Model =
    { board : Idx
    , locked : Bool
    }


type alias Idx =
    Dict.Dict Coord Cell


type alias Coord =
    ( Int, Int )


type Cell
    = Empty CellState
    | Mine CellState


type CellState
    = Initial AdjMines
    | Explored AdjMines
    | Flagged AdjMines


type alias AdjMines =
    Int


pt x y =
    ( x, y )


mineSize =
    12


isMineAt : Idx -> Coord -> Bool
isMineAt board ( x, y ) =
    case Dict.get ( x, y ) board of
        Just (Mine _) ->
            True

        _ ->
            False


getPointsAround : Coord -> List Coord
getPointsAround ( x, y ) =
    [ ( x - 1, y )
    , ( x, y - 1 )
    , ( x - 1, y - 1 )
    , ( x + 1, y - 1 )
    , ( x - 1, y + 1 )
    , ( x + 1, y )
    , ( x, y + 1 )
    , ( x + 1, y + 1 )
    ]


getNumberOfAjacentMines : Idx -> Coord -> Int
getNumberOfAjacentMines board coord =
    let
        isMine =
            isMineAt board
    in
    List.foldl
        (\adjCoord sum ->
            if isMine adjCoord then
                sum + 1

            else
                sum
        )
        0
        (getPointsAround coord)


sometimesMine : Random.Generator Cell
sometimesMine =
    Random.weighted ( 15, Mine (Initial -1) ) [ ( 85, Empty (Initial -1) ) ]


randomCells : Int -> Random.Generator (List Cell)
randomCells size =
    Random.list (size * size) sometimesMine


fromIndexToPt : Int -> Coord
fromIndexToPt i =
    let
        x =
            i // mineSize

        y =
            Basics.modBy mineSize i
    in
    pt x y


transformCellsToBoard : List Cell -> Idx
transformCellsToBoard cells =
    Dict.fromList
        (List.indexedMap (\i c -> ( fromIndexToPt i, c )) cells)


updateCellStateWithMineNum : Int -> CellState -> CellState
updateCellStateWithMineNum mn cs =
    case cs of
        Initial _ ->
            Initial mn

        Explored _ ->
            Explored mn

        Flagged _ ->
            Flagged mn


updateCellWithMineNum : Int -> Cell -> Cell
updateCellWithMineNum mn c =
    let
        updateCellState =
            updateCellStateWithMineNum mn
    in
    case c of
        Empty cs ->
            Empty (updateCellState cs)

        Mine cs ->
            Mine (updateCellState cs)


populateBoardWithMineNum : Idx -> Idx
populateBoardWithMineNum board =
    let
        getNumMinesForBoard =
            getNumberOfAjacentMines board
    in
    Dict.map
        (\coord cell ->
            let
                mineNum =
                    getNumMinesForBoard coord
            in
            updateCellWithMineNum mineNum cell
        )
        board


randomBoard : Random.Generator Idx
randomBoard =
    randomCells mineSize
        |> Random.map transformCellsToBoard
        |> Random.map populateBoardWithMineNum


generateIdxList : Int -> List ( Coord, Cell )
generateIdxList size =
    let
        listONums =
            List.range 0 (size - 1)
    in
    listONums
        |> List.map
            (\xCoord ->
                let
                    mk =
                        pt xCoord
                in
                listONums
                    |> List.map (\yCoord -> ( mk yCoord, Empty (Initial -1) ))
            )
        |> List.foldr (++) []


mapAtIndex : (Cell -> Cell) -> Coord -> Idx -> Idx
mapAtIndex mapper coord board =
    Dict.map
        (\key val ->
            if key == coord then
                mapper val

            else
                val
        )
        board


initialIdx : Idx
initialIdx =
    generateIdxList mineSize
        |> Dict.fromList
        |> mapAtIndex (\_ -> Mine (Initial -1)) ( 0, 0 )


init : ( Model, Cmd Msg )
init =
    ( { board = initialIdx, locked = False }, Random.generate NewBoard randomBoard )



---- UPDATE ----


type Msg
    = NoOp
    | RequestNewBoard
    | NewBoard Idx
    | FlagCell Coord
    | UnflagCell Coord
    | ChooseCell Coord


toggleFlagCell c coord =
    case c of
        Empty (Flagged adjMin) ->
            UnflagCell coord

        Mine (Flagged adjMin) ->
            UnflagCell coord

        Empty (Initial adjMin) ->
            FlagCell coord

        Mine (Initial adjMin) ->
            FlagCell coord

        _ ->
            NoOp


flagAt ( targetX, targetY ) ( x, y ) cell =
    if targetX == x && targetY == y then
        case cell of
            Empty (Initial adjMin) ->
                Empty (Flagged adjMin)

            Empty (Explored adjMin) ->
                Empty (Flagged adjMin)

            Mine (Initial adjMin) ->
                Mine (Flagged adjMin)

            Empty cs ->
                Empty cs

            Mine cs ->
                Mine cs

    else
        cell


unflagAt ( targetX, targetY ) ( x, y ) cell =
    if targetX == x && targetY == y then
        case cell of
            Empty (Flagged adjMin) ->
                Empty <| Initial adjMin

            Mine (Flagged adjMin) ->
                Mine <| Initial adjMin

            _ ->
                cell

    else
        cell


extractMineNum : CellState -> AdjMines
extractMineNum cs =
    case cs of
        Initial mineNum ->
            mineNum

        Flagged mineNum ->
            mineNum

        Explored mineNum ->
            mineNum


exploreAt ( targetX, targetY ) ( x, y ) cell =
    if targetX == x && targetY == y then
        case cell of
            Empty cs ->
                extractMineNum cs
                    |> Explored
                    |> Empty

            Mine cs ->
                extractMineNum cs
                    |> Explored
                    |> Mine

    else
        cell


exploreCoord : Coord -> Idx -> Idx
exploreCoord coordToExplore board =
    Dict.update coordToExplore
        (\mc ->
            case mc of
                Just cell ->
                    exploreAt coordToExplore coordToExplore cell
                        |> Just

                Nothing ->
                    Nothing
        )
        board


hasZeroMines : Idx -> Coord -> Bool
hasZeroMines board cell =
    0 == getNumberOfAjacentMines board cell


getZeroesAround : Idx -> Coord -> List Coord
getZeroesAround board cell =
    getPointsAround cell
        |> List.filter (hasZeroMines board)


isAlreadyIncluded : Set.Set Coord -> List Coord -> Bool
isAlreadyIncluded existingSet newCoords =
    Set.fromList newCoords
        |> Set.diff existingSet
        |> Set.isEmpty


inBounds : Coord -> Bool
inBounds ( x, y ) =
    x >= 0 && y >= 0 && x <= mineSize && y <= mineSize


recurseZeroesAround : Idx -> Coord -> Set.Set Coord -> Set.Set Coord
recurseZeroesAround board coord foundCells =
    let
        set =
            if hasZeroMines board coord then
                Set.insert coord foundCells

            else
                foundCells

        aroundZeroes =
            getZeroesAround board coord
                |> List.filter (\newZeroCoord -> not <| Set.member newZeroCoord set)
                |> List.filter inBounds
    in
    if List.isEmpty aroundZeroes || isAlreadyIncluded set aroundZeroes then
        set

    else
        aroundZeroes
            |> List.foldl (recurseZeroesAround board) set


zeroesAround : Idx -> Coord -> Set.Set Coord
zeroesAround board coord =
    let
        cellAt =
            Dict.get coord board
    in
    if hasZeroMines board coord then
        recurseZeroesAround board coord Set.empty

    else
        Set.empty


addAroundList : Set.Set Coord -> List Coord
addAroundList coordSet =
    Set.foldl (\coord previousCoordSet -> getPointsAround coord |> List.foldl Set.insert previousCoordSet) Set.empty coordSet
        |> Set.union coordSet
        |> Set.toList
        |> List.filter inBounds


exploreFoundSafeRegion : Idx -> Coord -> Idx
exploreFoundSafeRegion board coord =
    zeroesAround board coord
        |> addAroundList
        |> List.foldl exploreCoord board


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        handleLock m a =
            if m.locked then
                ( model, Cmd.none )

            else
                a
    in
    case msg of
        NoOp ->
            ( model, Cmd.none )

        RequestNewBoard ->
            ( model, Random.generate NewBoard randomBoard )

        NewBoard newboard ->
            ( { model | board = newboard, locked = False }, Cmd.none )

        FlagCell coord ->
            handleLock model ( { model | board = Dict.map (flagAt coord) model.board }, Cmd.none )

        UnflagCell coord ->
            handleLock model ( { model | board = Dict.map (unflagAt coord) model.board }, Cmd.none )

        ChooseCell coord ->
            let
                exploredBoard =
                    if hasZeroMines model.board coord then
                        exploreFoundSafeRegion model.board coord

                    else
                        Dict.map (exploreAt coord) model.board
            in
            if isMineAt model.board coord then
                handleLock model ( { model | locked = True, board = exploredBoard }, Cmd.none )

            else if hasZeroMines exploredBoard coord then
                handleLock model ( { model | board = exploreFoundSafeRegion exploredBoard coord }, Cmd.none )

            else
                handleLock model ( { model | board = exploredBoard }, Cmd.none )



---- VIEW ----


renderCell : Idx -> ( Coord, Cell ) -> Element Msg
renderCell board ( ( x, y ), c ) =
    let
        ( txt, color ) =
            case c of
                Empty e ->
                    case e of
                        Initial _ ->
                            ( " ", white )

                        Flagged _ ->
                            ( "🚩", white )

                        Explored adjMines ->
                            ( String.fromInt adjMines, white )

                Mine m ->
                    case m of
                        Initial _ ->
                            ( " ", white )

                        Flagged _ ->
                            ( "🚩", white )

                        Explored _ ->
                            ( "💣", red )
    in
    el
        [ Border.solid
        , Background.color color
        , Border.color black
        , Border.width 1
        , width cellSize
        , height cellSize
        , padding 6
        , spacing 0
        , onRightClick <| toggleFlagCell c ( x, y )
        , onClick <| ChooseCell ( x, y )
        , pointer
        , mouseOver [ Background.color hoverBlue ]
        ]
        (el [ centerX ] (text txt))


headingRow : Element Msg
headingRow =
    row [ width <| px 300, centerX, padding 20 ]
        [ el [ centerX, Font.size 50 ] (text "MineSweeper") ]



-- seems pretty awkward to generate this nested structure,
--  maybe we shold reconsider the model?


toRows : Idx -> List Int
toRows board =
    let
        dupeRows =
            Dict.keys board
                |> List.map (\( rowNum, _ ) -> rowNum)
    in
    Set.fromList dupeRows
        |> Set.toList


renderRows : Idx -> List Int -> List (Element Msg)
renderRows board =
    let
        getRowOfCells num =
            Dict.filter (\( row, _ ) _ -> row == num)

        toCellList rowNum =
            getRowOfCells rowNum board
                |> Dict.toList
                |> List.map (renderCell board)
    in
    List.map
        (\rowNum -> row [ centerX, centerY, spacing 0 ] (toCellList rowNum))


gameRow : Model -> Element Msg
gameRow model =
    column [ centerX, width fill ]
        (renderRows model.board (toRows model.board))


won : Idx -> Bool
won =
    Dict.foldl
        (\_ c winning ->
            if winning then
                case c of
                    Empty (Initial _) ->
                        False

                    Mine (Initial _) ->
                        False

                    _ ->
                        True

            else
                winning
        )
        True


overview : Model -> Element Msg
overview model =
    column [ width fill, centerX ]
        [ headingRow
        , row [ centerX, Font.size 20, pointer, onClick <| RequestNewBoard ] [ el [] <| text "New board 🔄" ]
        , if model.locked then
            row [ centerX, Font.size 30, paddingEach { top = 15, left = 0, bottom = 25, right = 0 } ] [ el [] (text "You have lost") ]

          else if won model.board then
            row [ centerX, Font.size 32, paddingEach { top = 15, left = 0, bottom = 25, right = 0 } ] [ el [] (text "You won!") ]

          else
            el [] (text "")
        , gameRow model
        ]



---- PROGRAM ----


view : Model -> Html Msg
view model =
    overview model
        |> Element.layout []


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
